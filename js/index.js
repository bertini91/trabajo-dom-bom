let num = 0;
let exerContent = document.getElementById("exerContent");
let butttonE4 = document.getElementById("buttonE4");
let reloj;
let tiempoCronometro;
let segundosCronometro = 0;
let minutosCronometro = 0;
let horasCronometro = 0;
let timeHoursE5 = 0;
let timeMinutesE5 =0;
let timeSecondsE5 = 0;

function exercise1(){
    window.clearInterval(reloj);
    document.body.style.backgroundImage = "none"; 
    num = Math.round(Math.random()*10);
    console.log(num);
    butttonE4.innerHTML=" ";
    exerContent.innerHTML = `<h2>Ejercicio 1 - Adivinar Numero Aleatorio del 1 al 10</h2><hr>`;
    exerContent.innerHTML += `<input id="valueE1" type="number" class="form-control col-2 m-4 inputE1-2" placeholder="Ingrese el Numero"> <br>`;
    exerContent.innerHTML += `<button id="confirmEjer1" type="button" class="btn btn-info ml-4" onclick="compareValue()">Enviar</button>`;
    exerContent.innerHTML += `<button type="button" class="btn btn-info ml-2" onclick="window.location.reload()">Atras</button>`;
}
function compareValue(){
    let valueInput = document.getElementById("valueE1").value;
    if(valueInput>=0 && valueInput<=10){
        if(num != valueInput){
            if(num > valueInput){
                alert("No Adiviniaste... el numero es mayor. Siga participando!! :)");
            }else{
                alert("No Adiviniaste... el numero es menor. Siga participando!! :)");
            }
        }else{
            alert("Adivinaste! Que grandeee :D");
        }
    }else{
        exerContent.innerHTML +=`
                <div class="alert alert-secondary" role="alert">
                    El numero debe ser del 0 al 10
                </div>`;
    }
}

class Persona{
    constructor(nombre, dni, sexo, peso, altura, fecNac){
        this.nombre = nombre;
        this.edad = 2020-fecNac;
        this.dni = dni;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        this.fecNac = fecNac;
    }
    get mostrarNombre(){
        return this.nombre;
    }
    mostrarGeneracion(){
        switch(true){
            case this.fecNac >= 1930 && this.fecNac <= 1948:
                alert(`${this.nombre} es de la Generacion Silent Generation.
                    El rasgo caracteristico es de Austeridad`);
                console.log(`${this.nombre} es de la Generacion Silent Generation`);
                console.log(`El rasgo caracteristico es de Austeridad`);
                break;
            case this.fecNac >= 1949 && this.fecNac <= 1968:
                alert(`${this.nombre} es de la Generacion Baby Boom. 
                    El rasgo caracteristico es de Ambicion`)
                console.log(`${this.nombre} es de la Generacion Baby Boom`);
                console.log(`El rasgo caracteristico es de Ambicion`);
                break;
            case this.fecNac >= 1969 && this.fecNac <= 1980:
                alert(`${this.nombre} es de la Generacion X. 
                El rasgo caracteristico es de -obsesion por el Exito`);
                console.log(`${this.nombre} es de la Generacion X`);
                console.log(`El rasgo caracteristico es de -obsesion por el Exito`);
                break;
            case this.fecNac >= 1981 && this.fecNac <= 1993:
                alert(`${this.nombre} es de la Generacion Y. El rasgo caracteristico es de Frustacion`);
                console.log(`${this.nombre} es de la Generacion Y`);
                console.log(`El rasgo caracteristico es de Frustacion`);
                break;
            case this.fecNac >= 1994 && this.fecNac <= 2010:
                alert(`${this.nombre} es de la Generacion Z. El rasgo caracteristico es de Irreverencia`);
                console.log(`${this.nombre} es de la Generacion Z`);
                console.log(`El rasgo caracteristico es de Irreverencia`);
                break;
            default:
                alert("no reconoce nada!");
                break;
        }
    }
    esMayorDeEdad(){
        if(2020 - this.fecNac >= 18){
            alert(`${this.nombre} SI es mayor de edad`);
        }else{
            alert(`${this.nombre} NO es mayor de edad`);
        }
    }
    mostrarDatos(){
        alert(
            alert(`
                <strong>DATOS PERSONALES </strong>
                Nombre: ${this.nombre}. 
                Edad: ${this.edad}. 
                DNI: ${this.dni}. 
                Sexo: ${this.sexo}. 
                Peso: ${this.peso}Kg. 
                Altura: ${this.altura}mtr.
                Nacimiento: ${this.fecNac}. 
            `)
        );
        console.log(`Nombre: ${this.nombre}`);
        console.log(`Edad: ${this.edad}`);
        console.log(`DNI: ${this.dni}`);
        console.log(`Sexo: ${this.sexo}`);
        console.log(`Peso: ${this.peso}Kg`);
        console.log(`Altura: ${this.altura}mtr`);
        console.log(`Fecha de Nacimiento: ${this.fecNac}`);
    }

}

function loadExercise2(){
    window.clearInterval(reloj);
    butttonE4.innerHTML=" ";
    document.body.style.backgroundImage = "none";
    exerContent.innerHTML = `<h2>Ejercicio 2 - Ingrese los siguientes datos!</h2><hr>`;
    exerContent.innerHTML += `<input id="nameE1" type="text" class="form-control col-2 m-4 inputE1-2" placeholder="Nombre">
    <input id="dniE1" type="text" class="form-control col-2 m-4 inputE1-2" placeholder="dni">
    <div class="form-check form-check-inline ml-4">
        <input id="masE1" class="form-check-input inputE1-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
        <label class="form-check-label" for="inlineRadio1">Masculino</label>
    </div>
    <div class="form-check form-check-inline ml-2">
        <input id="femE1" class="form-check-input inputE1-2" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
        <label class="form-check-label" for="inlineRadio2">Femenino</label>
    </div>
    <input id="pesE1" type="text" class="form-control col-2 m-4 inputE1-2" placeholder="Peso (kg)">
    <input id="altE1" type="text" class="form-control col-2 m-4 inputE1-2" placeholder="Altura (mtrs)">
    <input id="yearE1" type="text" class="form-control col-2 m-4 inputE1-2" placeholder="Año de Nac.">`
    exerContent.innerHTML += `<button type="button" class="btn btn-info ml-4" onclick="processExercise2()">Enviar</button>`;
    exerContent.innerHTML += `<button type="button" class="btn btn-info ml-2" onclick="window.location.reload()">Atras</button>`;
}
function processExercise2(){
    let nombre = document.getElementById("nameE1").value;
    let dni = document.getElementById("dniE1").value;
    let sexo;
    if(document.getElementById("masE1").checked == true){
        sexo = "M";
    }else{
        sexo = "F";
    }
    let peso = document.getElementById("pesE1").value;
    let altura = document.getElementById("altE1").value;
    let year = document.getElementById("yearE1").value;
    let per = new Persona(nombre, dni, sexo, peso, altura, year);
    per.mostrarGeneracion();
    per.esMayorDeEdad();
    per.mostrarDatos();
}

function loadExercise3(){
    butttonE4.innerHTML=" ";
    document.body.style.backgroundImage = "url('../img/fondoReloj.jpg')";
    reloj = setInterval(processExercise3, 1000);
}
function processExercise3(){
    console.log("Entro al processExercise2()");
    let fecha = new Date();
    let dia = obtenerDia(fecha.getDay());
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio",
        "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    let amPm;
    let horas = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    if(horas>12){
        if((horas-12) < 10){
            horas = "0"+ (horas-12);
        }else{
            horas = horas-12;
        }
    }

    switch(true){
        case (horas>=0 && horas<1):
            horas = (horas+12);
            break;
        case (horas>=13 && horas<0):
            horas = (horas-12);
            break;
    }

    if(minutos<10){
        minutos= "0"+ minutos;
    }
    if(segundos < 10){
        segundos = "0"+ segundos;
    }
 
    if(fecha.getHours() >= 12){
        amPm = "PM";
    }else{
        amPm = "AM";
    }
    exerContent.innerHTML = 
    `<div class="col-4 d-flex justify-content-center bg-secondary text-white ml-4 clockDate"> 
            <h3 class="mt-3 mb-3">${dia} ${fecha.getDate()} de ${meses[fecha.getMonth()]} del ${fecha.getFullYear()} </h3>
    </div>
    <div class="col-4 d-flex justify-content-center bg-secondary text-white mt-2 ml-4 clock">
        <div class="col-8 d-flex mr-0 pr-0">
              <p class="dateE3">${horas} :</p>
              <p class="dateE3"> ${minutos} :</p>
        </div>
        <div class="col-1 ml-0 pl-0">
              <h3 class="mb-0 mt-3">${amPm} </h3>
              <h3 class="mt-0">${segundos} </h3>
        </div>
    </div>`
}

function obtenerDia(dia){
    switch(true){
        case dia == 0:
            return "Domingo";
        case dia == 1:
            return "Lunes";
        case dia == 3:
            return "Martes";
        case dia == 4:
            return "Miercoles";
        case dia == 5:
            return "Jueves";
        case dia == 6:
            return "Viernes";
        case dia == 7:
            return "Sabado";
    }
}


function loadExercise4(){
    window.clearInterval(reloj);
    writeChronometer();
    writeButtons();
}

function processExercise4(){
    switch(true){
        case segundosCronometro == 60:
            segundosCronometro = 0;
            minutosCronometro = minutosCronometro +1;
            break;
        case minutosCronometro == 60:
            minutosCronometro = 0;
            segundosCronometro = 0;
            horasCronometro = horasCronometro +1;
            break;
    }
    writeChronometer();
    segundosCronometro++;
    
}
function writeButtons(){
    butttonE4.innerHTML = 
    `<div class="d-flex justify-content-between chronometer buttonTime">
        <button type="button" class="btn btn-info" onclick="startChronometer()">Iniciar</button>
        <button type="button" class="btn btn-info" onclick="pauseChronometer()">Pausar</button>
        <button type="button" class="btn btn-info" onclick="restartChronometer()">Stop</button>
    </div>`;
}
function writeChronometer(){
    exerContent.innerHTML = `
        <section class="container justify-content-center w-75 mt-5 ml-5 mr-5">
            <div class="blue d-flex justify-content-between chronometer">
                <p>${horasCronometro}</p>
                <p>:</p>
                <p>${minutosCronometro}</p>
                <p>:</p>
                <p>${segundosCronometro}</p>
            </div>
        </section>`;
}
function startChronometer(){
    tiempoCronometro = window.setInterval(processExercise4, 1000);
}

function pauseChronometer(){
    window.clearInterval(tiempoCronometro);
}

function restartChronometer(){
    window.clearInterval(tiempoCronometro);
    segundosCronometro = 0;
    minutosCronometro = 0;
    horasCronometro = 0;
    console.log(`${horasCronometro} : ${minutosCronometro} : ${segundosCronometro}`);
    writeChronometer();
}

/* 
    Realizar una web con un temporizador donde el usuario pueda ingresar un tiempo desde donde comenzará a 
    decrementar el contador. Debe contener los botones, iniciar, pausar y reset.
 */

 /* initialTimeE5 */

function loadExercise5(){
    window.clearInterval(reloj);
    
    exerContent.innerHTML = `
    <div class="d-flex justify-content-center col-12 timerE5">
        <input id="timeHoursE5" type="number" class="form-control col-2" placeholder="Hora" min="0" max="60">
        <input id="timeMinutesE5" type="number" class="form-control col-2" placeholder="Minutos" min="0" max="60">
        <input id="timeSecondsE5" type="number" class="form-control col-2" placeholder="Segundos" min="0" max="60">
    </div>`;
    butttonE4.innerHTML = 
    `<div class="d-flex justify-content-between chronometer buttonTime">
        <button type="button" class="btn btn-info" onclick="startTimer()">Iniciar</button>
        <button type="button" class="btn btn-info" onclick="pauseTimer()">Pausar</button>
        <button type="button" class="btn btn-info" onclick="restartTimer()">Stop</button>
    </div>`;
}
function processExercise5(){
    exerContent.innerHTML = `
        <section class="container justify-content-center w-75 mt-5 ml-5 mr-5">
            <div class="blue d-flex justify-content-between chronometer">
                <p>${timeHoursE5}</p>
                <p>:</p>
                <p>${timeMinutesE5}</p>
                <p>:</p>
                <p>${timeSecondsE5}</p>
            </div>
        </section>`;

        if(timeSecondsE5==0){
            switch(true){
                case timeMinutesE5 >0:
                    timeMinutesE5--;
                    timeSecondsE5=60;
                    break;
                case timeHoursE5 >0:
                    timeHoursE5--;
                    timeMinutesE5=59;
                    timeSecondsE5=60;
                    break;
                default:
                    alert("Tiempo Completado!!");
                    window.clearInterval(tiempoCronometro);
                    loadExercise5();
                    break;
            }
        }
        timeSecondsE5--;
    
}

function startTimer(){
    console.log(`ANTES __ Segundos = ${timeSecondsE5} - Minutos = ${timeMinutesE5} - Horas = ${timeHoursE5}`);
    if(timeHoursE5 == ""){
        timeHoursE5 =0;
    }
    if(timeMinutesE5 == ""){
        timeMinutesE5 = 0;
    }
    if(timeSecondsE5 == ""){
        timeSecondsE5 =0;
    }
    if(timeHoursE5 == 0 && timeMinutesE5 == 0 && timeSecondsE5 == 0){     //Obligadamente debo hacer esto para no volver a cargar al reiniciar
        console.log("SI INGRESA")
        timeHoursE5 = document.getElementById("timeHoursE5").value;
        timeMinutesE5 = document.getElementById("timeMinutesE5").value;
        timeSecondsE5 = document.getElementById("timeSecondsE5").value; 
    }

    console.log(`Segundos = ${timeSecondsE5} - Minutos = ${timeMinutesE5} - Horas = ${timeHoursE5}`);
    tiempoCronometro = window.setInterval(processExercise5, 1000);
    //Falta mostrar El temporizador y el decrementador
}
function pauseTimer(){
    window.clearInterval(tiempoCronometro);
}
function restartTimer(){
    window.clearInterval(tiempoCronometro);
    timeSecondsE5 = 0;
    timeMinutesE5 = 0;
    timeHoursE5 = 0;
    loadExercise5();
}